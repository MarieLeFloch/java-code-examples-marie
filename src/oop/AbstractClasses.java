package oop;

// A class that contains an abstract method must also be abstract and cannot be instanciated.
abstract class AbstractVehicle {
    protected double speed;

    public double getCurrentSpeed() {
        return this.speed;
    }

    // The constructor of an abstract class is only called when a child class is instanciated.
    public AbstractVehicle() {
        this.speed = 0;
    }

    // This is an _abstract method_. It has no body, but it is present on the type Vehicle. The body must be provided by all classes that
    // inherit from Vehicle.
    public abstract void accelerate();
}

class Bicycle extends AbstractVehicle {
    // Here we are forced to provide an implementation of this method. If we don't this class will also have to be abstract and it won't be
    // possible to instanciate it.
    public void accelerate() {
        this.speed = this.speed * 1.1;
        if(this.speed == 0) {
            this.speed = 1;
        }
    }
}

// Another class that extends the Vehicle class.
class Bus extends AbstractVehicle {
    public void accelerate() {
        this.speed = this.speed + 0.5;
    }
}


public class AbstractClasses {
    public static void main(String[] args) {
        AbstractVehicle v1 = new Bus();
        AbstractVehicle v2 = new Bicycle();

        // We can call the accelerate method on variables of type AbstractVehicle because the method exists in this class.
        // The correct implementation will be called based on the object inside the variable. This is called _polymorphism_.
        v1.accelerate();
        v1.accelerate();
        v2.accelerate();
        v2.accelerate();

        System.out.println(v1.getCurrentSpeed());
        System.out.println(v2.getCurrentSpeed());
    }
}
